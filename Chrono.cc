/* 
    xfem : C++ Finite Element Library
    developed under the GNU Lesser General Public License
    See the NOTICE, CONTRIBUTORS & LICENSE files for conditions.
*/
#ifndef CHRONO_CC
#define CHRONO_CC

// ----------------------------------------------------------------------------
// HEADERS
// ----------------------------------------------------------------------------
#include "Chrono.h"
#include <iostream>
#include <iomanip>
#include <map>

//#include <sys/times.h>
#include <unistd.h>

using namespace std;


// ----------------------------------------------------------------------------
// Chrono::Chrono()
// ----------------------------------------------------------------------------
Chrono::Chrono() 
{ 
  struct timezone tz;
  gettimeofday( &start, &tz ); 
}
#endif

// ----------------------------------------------------------------------------
// Chrono::top()
// ----------------------------------------------------------------------------
double Chrono::top() const
{
  struct timezone tz;
  timeval current;
  gettimeofday( &current, &tz );

  double scale = 1.0e-6;
  double val = static_cast<double>( current.tv_sec - start.tv_sec ) + 
               scale * static_cast<double>( current.tv_usec - start.tv_usec );
  return val;
}

