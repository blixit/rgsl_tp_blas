#include <iostream>
#include <cstdlib>
#include <cstdio>
#include <cmath>
#include <stdarg.h>     /* va_list, va_start, va_arg, va_end */ 
#include <fstream>      // std::ifstream
#include <sstream>		//stringstream

using namespace std;

#include "matrice_manager.hpp"
#include "Chrono.h"
		
/**
*	Constructeur 
*	@param n : shortdim
*	@param c : Lettre identifiant la matrice
*/
Matrice::Matrice(const int& n,char c){
	this->short_dim = n;
	this->dim = n*n;
	this->letter = c; 
	this->owner = true;
	this->matrice = new double[this->dim]; 
	for (int i = 0; i < this->dim; ++i){
		this->matrice[i] = 0; 
	} 
};
		
/**
*	Constructeur
*	@param A : pointeur contenant la matrice à copier
*	@param n : shortdim
*	@param c : Lettre identifiant la matrice
*/
Matrice::Matrice(double* A,const int& n,char c){
	this->short_dim = n;
	this->dim = n*n;
	this->letter = c;
	this->owner = false;
	this->matrice = A; 
}

/**
*	Constructeur 
*	@param M : une instance de la classe Matrice
*	@param c : Lettre identifiant la matrice
*/
Matrice::Matrice(Matrice& M, char c){
	this->short_dim = M.short_dim;
	this->dim = M.dim;
	this->letter = c != 0 ? c : M.letter; 
	this->owner = true;
	this->matrice = new double[this->dim];
	double* A = M.getMatrice(); 
	for (int i = 0; i < this->dim; ++i){
		this->matrice[i] = A[i]; 
	} 
}

/**
*	Destructeur  
*/
Matrice::~Matrice(){
	this->destroyMat();
	cout << "matrice  : " << this->letter <<  " destroyed." << endl;
}

/**
*	Initialise une matrice carrée de taille-éléments. 
*	Ex : pour taille = 9, on aura une matrice 3x3
*	@param taille : nombre d'éléments de la matrice.
*	@param ... : Liste des éléments séparés par des virgules 
*/
double* Matrice::initMat (int taille, ...){
	if(taille<=0)
		throw ("Bad parameters");

	this->destroyMat();
	this->matrice = new double[taille];  

	va_list vl;
	va_start(vl,taille);
	int cpt_arg = 0;
	for (int i=0;i<taille;i++)
	{
		this->matrice[i] = va_arg(vl,double);  
		cpt_arg++;
		//s'il y a plus d'argument que de cellules
		if(cpt_arg==taille)
			break;
	}
	va_end(vl); 
	return this->matrice;
}

/**
*	Détruit une matrice créée par l'instance actuelle.  
*/
void Matrice::destroyMat(){
	if(this->owner){
		if(this->matrice)
			delete []this->matrice;
	}
}


/**
*	Retourne un pointeur vers la matrice
*/
double* Matrice::getMatrice(){
	return this->matrice;
}

/**
*	Affiche une matrice.
*/
void Matrice::afficheMat(){
	if(this->matrice == NULL)
		throw "Echec affichage : La matrice est nulle.";
	cout << "[M] = " << this->letter << endl;
	for(int i=0; i<this->short_dim; i++){
		for(int j=0; j<this->short_dim; j++)
			cout << this->matrice[i+j*this->short_dim] << " ";
		cout << endl;	 
	}
	cout << endl;
}
void Matrice::affiche(double* M, int nx,int ny, char c){
	if(M == NULL)
		throw "Echec affichage : La matrice est nulle.";
	cout << "[M] = " << c << endl;
	for(int i=0; i<ny; i++){
		for(int j=0; j<nx; j++)
			cout << M[i+j*nx] << " ";
		cout << endl;	 
	}
	cout << endl;
}

/**
*	Affiche le vecteur donné en paramètre 
*	@param n : nombre d'éléments du vecteur.
*	@param V : pointeur vers le vecteur 
*	@param c : lettre identifiant le vecteur
*/
void Matrice::afficheVect(const int &n, double *V, char c){
	if(V == NULL)
		throw "Echec affichage : Le vecteur est nul.";
	cout << "[" << c << "] = " << endl;
	for (int i = 0; i < n; ++i) 
		cout << V[i] << " " << endl; 
	cout << endl;
}

/**
*	Retourne la matrice triangulaire inférieure 
*/
double* Matrice::getLower(){
	double* L = new double[this->short_dim*this->short_dim];
	for(int i(0);i<this->short_dim;i++)
	for(int j(0);j<this->short_dim;j++){
		if(i==j)
			L[i+j*this->short_dim] = 1;
		else if(i>j)
			L[i+j*this->short_dim] = this->matrice[i+j*this->short_dim];
		else
			L[i+j*this->short_dim] = 0;
	}
	return L;		
}

/**
*	Retourne la matrice triangulaire supérieure 
*/
double* Matrice::getUpper(){
	double* U = new double[this->short_dim*this->short_dim];
	for(int i(0);i<this->short_dim;i++)
	for(int j(0);j<this->short_dim;j++){
		if(i<=j)
			U[i+j*this->short_dim] = this->matrice[i+j*this->short_dim];
		else  
			U[i+j*this->short_dim] = 0;
	}
	return U;
}

/**
*	Retourne la matrice dans le format band_storage
*/
double* Matrice::to_bandFormat(int* diag_sup,int* dimx, int* dimy){
	*diag_sup=0;
	//Phase 1 : Recherche de la diagonale supérieure 
	/*
		On parcourt de haut en bas, diagonale par diagonale en partant de la diagonale
	supérieure. Si on trouve un élément non nul, on retourne le numéro de la diagonale 
	courante comme étant la diagonale supérieure.
	*/
	bool diagIsNull = true;
	int current_diag = this->getShortDim()-1, i, j;
	while(diagIsNull){
		j = current_diag;
		i = 0;

		for(int k(0);k<this->getShortDim()-current_diag;k++){
			if( this->matrice[i+j*this->getShortDim()] != 0){
				diagIsNull = false;
				*diag_sup = current_diag; 
				break;
			}
			j++;
			i++; 
		} 
		current_diag--;
		if(current_diag==0)
			break;
	}

	//Phase 2 : Création du tableau au bon format
	//diag_sup+1 nous donne le nombre de diagonales donc le nombre de lignes de la matrice au format band_storage
	double* band = new double[(*diag_sup+1)*this->short_dim];
	for(int i(0);i<this->short_dim;i++){
		for(int j(i);j<this->short_dim;j++){
			int i_band = (*diag_sup) - (j-i); //vertival_dim_band_matrix - element_diag 
			band[i_band+j*this->short_dim] = this->matrice[i+j*this->getShortDim()];
		}
	}
	*dimx = this->short_dim;
	*dimy = (*diag_sup+1);

	return band;
}


/**
*	Initialise une matrice à partir des données d'un fichier au format Market (.mtx)
*	@param fluxname : chemin vers le fichier
*	@param dimx : pointeur retournant la dimension horizontale de la matrice tel que lu dans le fichier.
*	@param dimy : pointeur retournant la dimension verticale de la matrice tel que lu dans le fichier.
*	@param nz_elems : pointeur retournant le nombre d'éléments non nuls de la matrice tel que lu dans le fichier.
*/
void Matrice::readv1(const char* fluxname, int *dimx, int *dimy, double *nz_elems){
	Chrono m_time;
	ifstream fichier;
	fichier.open(fluxname,ifstream::in);

	if(!fichier.is_open())
		throw "L'ouverture du fichier a échoué.";

	char line[256];
	int i=0,j=0,k = 0;
	double data = 0; 

	while(fichier.good()){
		fichier.getline(line,256); 
		if(line[0]=='%'){ 
			continue; 
		} 		

		//on lit la prmière ligne après les commentaires et on sort
		std::stringstream s(line); 
		s >> *dimx >> *dimy >> *nz_elems; 

		if(*dimx <= 0 || *dimy <=0 || *nz_elems==0) 
			throw "Les paramètres de la matrice n'ont pas pu être chargés."; 

		this->short_dim = *dimx;
		this->dim = *dimx*(*dimy);
		this->letter = 'M';
		
		this->destroyMat();
		this->matrice = new double[this->dim];

		if(this->matrice == NULL)
			throw "L'allocation de mémoire pour la matrice a échoué.";
		break;
		
	} 
	
	while(fichier.good() && k<*nz_elems){
		fichier.getline(line,56); 

		std::stringstream s(line); 
		s >> i >> j >> data;
		i-=1; j-=1;
		if(i+j*this->short_dim >= this->dim){ 
			throw "Taille dépassée";
		}
		this->matrice[i+j*this->short_dim] = data;  
		this->matrice[j+i*this->short_dim] = data;  //since the matrix are symmetrical
		k++;
	} 

	fichier.close();
	cout << "Temps de lecture du fichier : " << m_time.top() << "s. " << endl;
}
 




