/* 
    xfem : C++ Finite Element Library
    developed under the GNU Lesser General Public License
    See the NOTICE, CONTRIBUTORS & LICENSE files for conditions.
*/
#ifndef CHRONO_HH
#define CHRONO_HH

// ----------------------------------------------------------------------------
// HEADERS
// ----------------------------------------------------------------------------
#include <sys/times.h>
#include <sys/time.h>
#include <string>
#include <map>
#include <vector>

//#define USE_FINE_THREAD 1

#ifdef USE_FINE_THREAD
extern "C" {
#include <time.h>
}
#endif
// ----------------------------------------------------------------------------
// Chrono
// ----------------------------------------------------------------------------
/*! \class Chrono
    \ingroup Utilities
    \brief Process timing measurment.

    Chrono is a small class that stores the current time when an instance is
    created and provides a function returning the number of seconds elapsed
    since the instance creation.

  
*/
class Chrono
{
  public:
    Chrono();
    ~Chrono(){};

    double top() const; //!< Return elapsed time since object creation.
 private:
    timeval start;      //!< Creation time of the object.
    mutable std::map<std::string, double> times;
};
#endif
