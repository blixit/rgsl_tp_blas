#ifndef CLASS_MATRICE_HPP
#define CLASS_MATRICE_HPP

/**
	\class Matrice
    \ingroup Utilities
    \brief Gestion de matrices

	Matrice est une petite classe qui permet de stocker les matrices selon le format colonne. 
*/
class Matrice{
	public : 
		Matrice(const int& n,char c='M');   
		Matrice(double* A,const int& n,char c='M');
		Matrice(Matrice& M,char c=0);
		~Matrice();

		double* getMatrice();
		double* initMat (int taille,...); 
		void destroyMat();
		void afficheMat();
		static void affiche(double* M, int nx,int ny, char c='M');
		static void afficheVect(const int &n, double *V, char c='V');
		double* getLower();
		double* getUpper();
		double* to_bandFormat(int* diag_sup,int* dimx, int* dimy);

		void readv1(const char* fluxname, int* dimx, int* dimy, double* nz_elems);

		/** \brief Retourne la dimension réelle du tableau contenant la matrice. Ex: 9 pour une matrice 3*3*/
		int getDim(){return this->dim;};  
		/** \brief Retourne la dimension de la matrice. Ex: 3 pour une matrice 3*3*/
		int getShortDim(){return this->short_dim;};  
	private : 
		double * matrice;
		int dim;
		int short_dim;
		char letter;
		bool owner;
};

#endif
