#ifndef INCLUDED_ALGOLU_HPP
#define INCLUDED_ALGOLU_HPP

/*#include<iostream>
#include<stdlib.h>
#include<iomanip>
using namespace std;*/

/*void destroyMat(double* A);
void afficheMat(double *A, int n, char c='M');
double* getLower(double* A, int n);
double* getUpper(double* A, int n);*/
int dLU1b(const int &n, double *A,const int& LDA,double* elapsedTime);
int dLU2b(const int &n, double *A,const int& LDA,double* elapsedTime);
int dLU3b(const int &n, double *A,const int& LDA,const int& r,double* elapsedTime);
int solveAX(const int &N, const int &NRHS, double *A, const int &LDA, double *B, const int &LDB,double* elapsedTime);
int solve_2steps(const int &N, const int &NRHS, double *A, const int &LDA, double *B, const int &LDB,double* elapsedTime_Factorisation,double* elapsedTime_Resolution);


extern "C" void dscal_(const int &N, const double &DA, double* DX, const int &INCX);
extern "C" void dger_(const int &M,const  int &N, const double &ALPHA, double* X,const  int &INCX, double* Y, const int &INCY, double *A_LDA, const int &LDA);
extern "C" void dtrsm_(const char &SIDE,const char &UPLO,const char &TRANSA,const char &DIAG,const int &M,const int &N,const double &ALPHA,double *A,const int &LDA,double* B,const int &LDB ); 	
extern "C" void dgemm_(const char &TRANSA, const char &TRANSB, const int &M, const int &N,const int &K, const double &alpha, const double *A, const int &LDA, const double*B,const int &LDB, const double &beta, double *C, const int &LDC);
extern "C" void dgesv_( const int &N,const int &NRHS,double *A,const int &LDA,int *IPIV,double *B,const int &LDB,int &INFO );
extern "C" void dgetrf_(const int &M,const int &N,double *A,const int &LDA,int *IPIV,int &INFO);
extern "C" void dgetrs_( const char &TRANS,const int &N,const int &NRHS,double *A,const int &LDA,int *IPIV,double *B,const int &LDB,int &INFO);
extern "C" void dpbsv_(const char &UPLO,const int &N,const int &KD,const int &NRHS,double *AB,const int &LDAB,double *B,const int &LDB, int &INFO );
	
#endif
