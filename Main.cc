#include <stdlib.h>
#include <iostream>
#include <iomanip>
#include <exception>
#include <ctime> // time()

using namespace std;

#include"algolu.h"
#include "matrice_manager.hpp"




int main(int argc, char** argv)
{
	// 1ere Version 
	int n = 3;
	if(argc!=2){
		cout<<" Dimension de la matrice A ? Rentrez 3 pour l'exemple"<<endl;
		cin>>n;
	}
	else{
		 
	}
	/*Matrice A(n,'A');

	try{
		A.initMat (A.getDim(), 1.,1.,2.,1.,2.,4.,1.,3.,5.);  
	}catch(const char* e){
		cout << e << endl;
		exit(-1);
	}
	
	A.afficheMat();  
	cout << "Résultat : " << LU_Version_1(A.getShortDim(),A.getMatrice(),3) << endl;
	Matrice U(A.getUpper(),A.getShortDim(),'U');
	Matrice L(A.getLower(),A.getShortDim(),'L');

	A.afficheMat();
	L.afficheMat();
	U.afficheMat(); */

	Matrice A(5,'A'); 

	int dimx = 4,dimy = 4;
	double nz_elems	= 0;
	try{
		cout << "Chargement du fichier '" << argv[1] << "' ... "<< endl;
		A.readv1(argv[1],&dimx,&dimy,&nz_elems); 
		//A.initMat (A.getDim(), 10.,0.,1.,0.,0.,   0.,10.,3.,1.,0., 3.,0.,10.,0.,1. ,0.,0.,0.,10.,0. ,0.,1.,0.,0.,10. ); 
		//A.afficheMat(); 
		int diagsup = 0;
		double *D = A.to_bandFormat(&diagsup,&dimx,&dimy);
		//Matrice::affiche(D,dimx,dimy,'D');
		cout << "diagsup " << diagsup << " x " << dimx << " y " << dimy << endl;
		exit(-1);
	}catch(const char* e){
		cout << e << endl;
		exit(-1);
	}
	double elapsedTime = 0, elapsedTime_Factorisation = 0, elapsedTime_Resolution = 0;

	//vecteur B. Ce vecteur contient X après résolution
	int nhrs = 1;
	double *B = new double[dimx],*C = new double[dimx];
	srand(time(NULL));

	for (int i = 0; i < dimx; ++i)
	{
		B[i] = 1.0*(static_cast <double> (rand()) / (static_cast <double> (RAND_MAX/(1e5)))); 
		C[i] = B[i];//*(static_cast <double> (rand()) / (static_cast <double> (RAND_MAX/(1e5)))); 
	} 
	//Matrice::afficheVect(dimx,B,'B'); 
	Matrice M(A,'M'); 
	try{
		cout << "Méthode LU 1 " << endl;
		//cout << "Résultat : " << dLU1b(A.getShortDim(),A.getMatrice(),A.getShortDim(),&elapsedTime) << endl;
		cout << "Elalpsed time : " << elapsedTime <<  " s" << endl;
		cout << endl;
		cout << "Méthode LU 2 " << endl; 
		//cout << "Résultat : " << dLU2b(A.getShortDim(),A.getMatrice(),A.getShortDim(),&elapsedTime) << endl;
		cout << "Elalpsed time : " << elapsedTime <<  " s" << endl;
		cout << endl;
		cout << "Méthode LU 3 " << endl; 
		//cout << "Résultat : " << dLU3b(M.getShortDim(),M.getMatrice(),M.getShortDim(),1,&elapsedTime) << endl;
		cout << "Elalpsed time : " << elapsedTime <<  " s" << endl;
		cout << endl;
		cout << "Resolving using DGESV directly : " << endl; 
		//cout << "Résultat : " << solveAX(M.getShortDim(),nhrs,M.getMatrice(),M.getShortDim(),B,M.getShortDim(),&elapsedTime) << endl;
		cout << "Elalpsed time : " << elapsedTime <<  " s" << endl;
		cout << endl;
		cout << "Resolving using dgetrf et dgetrs : " << endl;
		//cout << "Résultat : " << solve_2steps(n,nhrs,A.getMatrice(),A.getShortDim(),C,A.getShortDim(),&elapsedTime_Factorisation,&elapsedTime_Resolution) << endl;
		cout << "Elalpsed time in factorisation : " << elapsedTime_Factorisation <<  " s" << endl;
		cout << "Elalpsed time in resolution : " << elapsedTime_Resolution <<  " s" << endl;
		cout << endl;
	}catch(const char* e){
		cout << e << endl; 
	}catch(exception &e){
		cout << e.what() << endl; 
	}
	//A.afficheMat();
	//Matrice U(A.getUpper(),A.getShortDim(),'U');
	//Matrice L(A.getLower(),A.getShortDim(),'L');
	//L.afficheMat();
	//U.afficheMat();


	//Matrice::afficheVect(dimx,B,'X'); 

	delete []B;

	return(0);
}




