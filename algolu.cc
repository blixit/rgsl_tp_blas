#include <iostream>
#include <cstdlib>
#include <cstdio>
#include <cmath>
#include <exception>
#include <cstring> // sprintf 

using namespace std;

#include"algolu.h"
#include"Chrono.h"

int dLU1b(const int &n, double *A,const int& LDA,double* elapsedTime){
	Chrono m_time;

	int k(0);
	const double minpiv = 1.e-6;
	double piv = A[0];
	while((fabs(piv) > minpiv) && (k < n-1)){
		for(int i(k+1);i<n;i++)
			A[i+k*LDA] /= piv;
		for(int i(k+1);i<n;i++)
			for(int j(k+1);j<n;j++)
				A[i+j*LDA] -= A[i+k*LDA]*A[k+j*LDA];
		k += 1;
		piv = A[k+k*LDA];
	}
	if(fabs(piv) <= minpiv){
		char erreur[64];
		sprintf(erreur,"[DLU1b] : Null pivot : %lf",piv); 
		throw erreur; 
	}

	*elapsedTime = m_time.top();

	return 1; 
}


int dLU2b(const int &n, double *A,const int& LDA,double* elapsedTime){
	Chrono m_time;

	int k(0);
	const double minpiv = 1.e-6; 
	double piv = A[0];  
	while((fabs(piv)>minpiv) && (k < n-1)){ 
		dscal_(n-(k+1),1./piv,A+(k+1+k*LDA),1); 
		dger_(n-(k+1),n-(k+1),-1.,A+(k+1+k*LDA),1,A+(k+(k+1)*LDA),LDA,A+(k+1+(k+1)*LDA),LDA);
		k++;
		piv = A[k+k*LDA];
	}
	if(fabs(piv) <= minpiv){
		char erreur[64];
		sprintf(erreur,"[DLU2b] : Null pivot : %lf",piv); 
		throw erreur; 
	} 
	*elapsedTime = m_time.top();

	return 1; 
}

int dLU3b(const int &n, double *A,const int& LDA,const int& r,double* elapsedTime){
	Chrono m_time;

	int l(0);  
	double localElapsedTime = 0;
	*elapsedTime = 0;
	while(l<n){
		int m = std::min(n,l+r);
		int bsize = m-l;
		int success = dLU2b(bsize,A+(l+l*LDA),LDA,&localElapsedTime);
		if(!success){
			throw "[DLU3b] : Can't factorise one block.";
		}
		*elapsedTime+=localElapsedTime;
		dtrsm_('L','L','N','U',bsize,n-m,1.,A+(l+l*LDA),LDA,A+l+m*LDA,LDA);
		dtrsm_('R','U','N','N',n-m,bsize,1.,A+(l+l*LDA),LDA,A+m+l*LDA,LDA);
		dgemm_('N','N',n-m,n-m,bsize,-1.,A+m+l*LDA,LDA,A+l+m*LDA,LDA,1.,A+m+m*LDA,LDA);
		l = m;  
	} 
	*elapsedTime = m_time.top();

	return 1; 
}

/*
Cette méthode a testé avec notre matrice 3*3 et le vecteur B = [1;1;1]. Le résultat vérifié à la main est bien X = [2;-2;1];
*/
int solveAX(const int &N, const int &NRHS, double *A, const int &LDA, double *B, const int &LDB,double* elapsedTime){
	Chrono m_time;
	int INFO;
	int *IPIV = new int[N];

	dgesv_( N,NRHS,A,LDA,IPIV,B,LDB,INFO );

	*elapsedTime = m_time.top();
	delete []IPIV;
	
	return INFO;
}

int solve_2steps(const int &N, const int &NRHS, double *A, const int &LDA, double *B, const int &LDB,double* elapsedTime_Factorisation,double* elapsedTime_Resolution){
	
	int INFO;
	int *IPIV = new int[N];
	
	//factorisation
	Chrono m_time;
	*elapsedTime_Factorisation = 0;
	dgetrf_( N, N, A, LDA, IPIV, INFO );
	*elapsedTime_Factorisation = m_time.top();

	//résolution si aucune erreur
	if(INFO == 0){
		Chrono m_time2;
		*elapsedTime_Resolution = 0;
		dgetrs_( 'N', N, NRHS, A, LDA, IPIV, B, LDB,INFO);
		*elapsedTime_Resolution = m_time2.top();
	}
	else{
		throw "La factorisation a échoué.";
	}

	
	delete []IPIV;
	
	return INFO;
}

int solve_band(const int &N, const int &NRHS,const Matrice &M, const int &LDA, double *B, const int &LDB,double* elapsedTime){
	int KD = 0,dimx,dimy; //diagsup
	double *A = M.to_bandFormat(&KD,&dimx,&dimy);
	*elapsedTime = 0;
	Chrono m_time2;
	dpbsv_('U',N,KD,NRHS,double *AB,const int &LDAB,double *B,const int &LDB, int &INFO );
	
	*elapsedTime = m_time.top();
}



